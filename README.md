# CarGame - Juego de Carreras

Vídeo: https://youtu.be/WFd-eWFIidU

Este proyecto es un juego de carreras desarrollado como parte de una evaluación. El juego se enfoca en un modo Contrarreloj (Time Trial), donde el jugador compite contra su propio récord en un circuito montañoso con partes urbanas.

## Ejecución del Proyecto

Para ejecutar el proyecto, sigue estos pasos:

1. Clona el repositorio desde GitLab.
2. Abre el proyecto en Unity 2022.3.20f1.
3. Asegúrate de tener todas las dependencias y configuraciones correctas.
4. Ejecuta el juego desde Unity o compila el proyecto y ejecuta el archivo ejecutable.

## Controles del Juego

Los controles del juego son simples:

- Flechas direccionales para moverse:
  - Arriba: Correr.
  - Abajo: Frenar.
  - Izquierda/Derecha: Moverse lateralmente.

## Puntos Implementados

### Uso de JSON para guardar información
El uso de JSON para almacenar datos del juego proporciona una forma eficiente y estructurada de guardar y cargar información relevante del juego, como la puntuación más alta del jugador, configuraciones de juego personalizadas, o cualquier otro dato necesario para mantener la progresión del jugador entre sesiones de juego.

### Añadir sonido y efectos de sonido (FX)
Se agregaron efectos de sonido para mejorar la experiencia del juego. Los efectos de sonido contribuyen a la inmersión del jugador y pueden utilizarse para indicar acciones importantes, como la colisión con obstáculos, el cambio de velocidad, etc.

### Leer los datos del fantasma de un archivo de texto usando Resources de Unity
Se implementó la lectura de datos del fantasma desde un archivo de texto utilizando la funcionalidad Resources de Unity. Esto permite al juego cargar y reproducir datos de recorridos anteriores del jugador o de otros competidores, lo que añade un elemento competitivo adicional y la posibilidad de mejorar las habilidades de conducción al observar y competir contra los registros anteriores.

### Repeticiones usando Cinemachine
Se han agregado diversas cámaras en el modo de repetición usando Cinemachine. Cinemachine es una herramienta poderosa para crear y controlar fácilmente las cámaras en juegos, permitiendo la creación de secuencias cinematográficas y la personalización de la experiencia visual del jugador durante las repeticiones, lo que añade un aspecto visualmente atractivo al juego y puede ayudar a los jugadores a analizar su rendimiento durante la carrera.

### Checkpoints
Permiten conocer que el jugador ha pasado por los puntos correctos para completar una vuelta.

## Material utilizado

- Efectos de sonido de Gran Turismo 2 y Gran Turismo 3.
- Canciones de Gran Turismo 2, Gran Turismo 3 y Mario Kart Wii.
